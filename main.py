
class ZadanieBase:
    def input(self):
        self.str_ = input('Введите число N:')

    def validate_isdigit(self):
        return self.str_.isdigit()

    def validate_iseven(self):
        return self.int_ % 2 != 0

    def data_convert(self):
        self.int_ = int(self.str_)


class ZadanieOne(ZadanieBase):

    def __init__(self):
        self.process_()

    def process_(self):
        self.input()
        ret = self.validate_isdigit()
        if not ret:
            raise ValueError('Вы ввели не число, ожидается число')
        self.data_convert()
        self.range()
        self.print_out()

    def range(self):
        self.range_list = list(range(1, self.int_+1))

    def print_out(self):
        print(self.range_list)


class ZadanieTwo(ZadanieBase):

    def __init__(self):
        self.process_()

    def process_(self):
        self.input()
        ret = self.validate_isdigit()
        if not ret:
            raise ValueError('Вы ввели не число, ожидается число')
        self.data_convert()
        ret = self.validate_iseven()
        if not ret:
            raise ValueError('Вы ввели чётное число, ожидается нечётное')
        self.print_out()

    def print_out(self):
        for row in range(1, self.int_ + 1):
            for col in range(1, self.int_ + 1):
                if row == self.int_ // 2 + 1 and col == self.int_ // 2 + 1:
                    print(" ", end="")
                else:
                    print("#", end="")
            print()



if __name__ == '__main__':
    ZadanieTwo()